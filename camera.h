#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "cglm/include/cglm/vec3.h"

typedef struct
{
    vec3 position;
    vec4 rotation;
} Camera;

Camera cameraCreate(void);
void cameraMove(Camera *camera, GLFWwindow *window, double deltaTime);

#endif
