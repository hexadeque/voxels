#ifndef ARENA_H
#define ARENA_H

#include <stddef.h>

typedef struct Arena Arena;

Arena *arenaCreate();
void *arenaAlloc(Arena *arena, size_t size);
void arenaDestroy(Arena *arena);

#endif
