#ifndef WINDOW_H
#define WINDOW_H

#include <GLFW/glfw3.h>

GLFWwindow *windowSetup(unsigned int screenWidth, unsigned int screenHeight);
void windowFrame(GLFWwindow *window);

#endif
