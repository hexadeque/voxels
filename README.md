# Voxel engine
A rendering engine using a raytracer in compute shaders to render voxels. For now, only the DDA algorithm and octree traversal is implemented, and there is no custom worlds or realistic rendering.

## Compiling and running
Dependencies:
- glew
- glfw3

```sh
git clone --recursive https://gitlab.com/hexadeque/voxels.git
make
./voxels
```