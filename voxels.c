#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "window.h"
#include "render.h"
#include "camera.h"
#include "world.h"

const unsigned int SCREEN_WIDTH = 1920;
const unsigned int SCREEN_HEIGHT = 1080;

int main(void)
{
    GLFWwindow *window = windowSetup(SCREEN_WIDTH, SCREEN_HEIGHT);
    Renderer *renderer = renderSetup(SCREEN_WIDTH, SCREEN_HEIGHT);
    Camera camera = cameraCreate();

    Arena *worldArena = arenaCreate();
    Octree world = worldCreate(7, worldArena);

    double lastTime = glfwGetTime();

    while (!glfwWindowShouldClose(window))
    {
        double time = glfwGetTime();
        double deltaTime = time - lastTime;
        lastTime = time;

        cameraMove(&camera, window, deltaTime);
        renderFrame(renderer, camera, world);
        windowFrame(window);

        printf("FPS: %f, %f, Postion: %f, %f, %f\n", 1.0 / deltaTime, deltaTime * 1000, camera.position[0], camera.position[1], camera.position[2]);
    }

    arenaDestroy(worldArena);

    renderCleanup(renderer);

    glfwTerminate();

    return 0;
}
