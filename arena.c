#include "arena.h"

#include <sys/mman.h>
#include <stdint.h>

struct Arena
{
    size_t pos;
    uint8_t buffer[];
};

const size_t ARENA_SIZE = 1000000000;

Arena *arenaCreate()
{
    Arena *arena = mmap(NULL, ARENA_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    arena->pos = 0;
    return arena;
}

void *arenaAlloc(Arena *arena, size_t size)
{
    void *start = arena->buffer + arena->pos;
    arena->pos += size;
    return start;
}

void arenaDestroy(Arena *arena)
{
    munmap(arena, ARENA_SIZE);
}
