CC=cc
CFLAGS=-g -Wall -Werror
LDFLAGS=-lglfw -lGL -lGLEW -lm
SRC=$(wildcard *.c)
OBJ=$(SRC:%.c=%.o)
TARGET=voxels

all: $(OBJ)
	$(CC) $^ -o $(TARGET) $(LDFLAGS)

%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	rm $(TARGET) *.o
