#include "world.h"
#include "octree.h"

#include <math.h>

const unsigned int STONE_RADIUS = 16;
const unsigned int STONE_HEIGHT = 17;

static VoxelType sphereGenerator(unsigned int x, unsigned int y, unsigned int z, void *data)
{
    unsigned int levels = *(unsigned int *) data;
    unsigned int width = pow(2, levels);
    unsigned int halfWidth = width / 2;

    float dx = (float) x - halfWidth;
    float dy = (float) y - STONE_HEIGHT;
    float dz = (float) z - halfWidth;

    if (dx * dx + dy * dy + dz * dz <= STONE_RADIUS * STONE_RADIUS)
    {
        return Stone;
    }

    return y == 0 ? Grass : Air;
}

Octree worldCreate(unsigned int levels, Arena *arena)
{
    return octreeCreate(sphereGenerator, levels, &levels, arena);
}
