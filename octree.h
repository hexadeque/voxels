#ifndef OCTREE_H
#define OCTREE_H

#include <stddef.h>
#include <stdbool.h>

#include "arena.h"

typedef enum 
{
    Air = 0,
    Mixed = 1,
    Stone = 2,
    Dirt = 3,
    Grass = 4,
} VoxelType;

typedef VoxelType (*WorldGenerator)(unsigned int x, unsigned int y, unsigned int z, void *data);

typedef struct OctreeNode OctreeNode;
struct OctreeNode
{
    VoxelType type;
    unsigned int children[8];
};

typedef struct
{
    OctreeNode *nodes;
    size_t nodesLength;
    unsigned int levels;
} Octree;

Octree octreeCreate(WorldGenerator worldGenerator, unsigned int depth, void *data, Arena *arena);

#endif
