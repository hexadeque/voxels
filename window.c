#include <stdlib.h>
#include <GL/glew.h>

#include "window.h"

GLFWwindow *windowSetup(unsigned int screenWidth, unsigned int screenHeight)
{
    if (!glfwInit())
    {
        exit(1);
    }

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    GLFWwindow *window = glfwCreateWindow(screenWidth, screenHeight, "Voxels", NULL, NULL);
    if (window == NULL)
    {
        glfwTerminate();
        exit(1);
    }

    glfwMakeContextCurrent(window);

    return window;
}

void windowFrame(GLFWwindow *window)
{
    glfwSwapBuffers(window);
    glfwPollEvents();
}
