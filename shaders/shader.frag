#version 430 core

uniform sampler2D screen;

in vec2 texCoord;

layout(location = 0) out vec4 color;

void main() {
    color = texture(screen, texCoord);
}
