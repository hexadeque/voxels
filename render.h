#ifndef RENDER_H
#define RENDER_H

#include "camera.h"
#include "octree.h"

typedef struct Renderer Renderer;

Renderer *renderSetup(unsigned int screenWidth, unsigned int screenHeight);
void renderFrame(Renderer *renderer, Camera camera, Octree world);
void renderCleanup(Renderer *renderer);

#endif
