#include "camera.h"

#include <string.h>
#include <GLFW/glfw3.h>
#include "cglm/include/cglm/quat.h"
#include "cglm/include/cglm/types.h"
#include "cglm/include/cglm/vec3.h"

static const float SENSITIVITY = 0.005f;
static const float SPEED = 40.0f;

Camera cameraCreate(void)
{
    Camera camera;
    glm_vec3_zero(camera.position);
    glm_quat_identity(camera.rotation);
    return camera;
}

void cameraMove(Camera *camera, GLFWwindow *window, double deltaTime)
{
    static double lastMouse[2];
    
    double mouse[2];
    glfwGetCursorPos(window, mouse, mouse + 1);

    vec3 forward = { 0, 0, 1 };
    glm_quat_rotatev(camera->rotation, forward, forward);

    vec3 up = { 0, 1, 0 };

    vec3 right;
    glm_vec3_cross(up, forward, right);

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        vec4 pitch;
        glm_quatv(pitch, (mouse[1] - lastMouse[1]) * SENSITIVITY, right);
        glm_quat_mul(pitch, camera->rotation, camera->rotation);

        vec4 yaw;
        glm_quat(yaw, (mouse[0] - lastMouse[0]) * SENSITIVITY, 0, 1, 0);
        glm_quat_mul(yaw, camera->rotation, camera->rotation);
    }
    else
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    glm_vec3_scale(forward, SPEED * deltaTime, forward);
    glm_vec3_scale(right, SPEED * deltaTime, right);
    glm_vec3_scale(up, SPEED * deltaTime, up);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        glm_vec3_add(camera->position, forward, camera->position);
    }

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        glm_vec3_sub(camera->position, forward, camera->position);
    }

    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        glm_vec3_add(camera->position, right, camera->position);
    }

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        glm_vec3_sub(camera->position, right, camera->position);
    }

    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
    {
        glm_vec3_add(camera->position, up, camera->position);
    }

    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
    {
        glm_vec3_sub(camera->position, up, camera->position);
    }

    memcpy(lastMouse, mouse, sizeof mouse);
}
