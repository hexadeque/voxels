#include "render.h"
#include "camera.h"
#include "octree.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct Renderer
{
    unsigned int screenWidth;
    unsigned int screenHeight;
    unsigned int computeProgram;
    unsigned int shaderProgram;
    unsigned int world_ssbo;
} Renderer;

const unsigned int WORLD_BINDING = 1;

const unsigned int ROTATION_LOCATION = 1;
const unsigned int POSITION_LOCATION = 2;
const unsigned int OCTREE_LEVELS_POSITION = 3;

static void APIENTRY debugCallback(
    unsigned int source,
    unsigned int type,
    unsigned int id,
    unsigned int severity,
    int length,
    const char *message,
    const void *data)
{
    char *sourceName;
    switch (source) {
        case GL_DEBUG_SOURCE_API:
            sourceName = "API";
            break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            sourceName = "WINDOW SYSTEM";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            sourceName = "SHADER COMPILER";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            sourceName = "THIRD PARTY";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            sourceName = "APPLICATION";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            sourceName = "UNKNOWN";
            break;
        default:
            sourceName = "UNKNOWN";
            break;
    }

    char *typeName;
    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            typeName = "ERROR";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            typeName = "DEPRECATED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            typeName = "UNDEFINED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            typeName = "PORTABILITY";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            typeName = "PERFORMANCE";
            break;
        case GL_DEBUG_TYPE_OTHER:
            typeName = "OTHER";
            break;
        case GL_DEBUG_TYPE_MARKER:
            typeName = "MARKER";
            break;
        default:
            typeName = "UNKNOWN";
            break;
    }

    char *severityName;
    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH:
            severityName = "HIGH";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            severityName = "MEDIUM";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            severityName = "LOW";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            severityName = "NOTIFICATION";
            return;
            break;
        default:
            severityName = "UNKNOWN";
            break;
    }

    printf("%d: %s of %s severity, raised from %s: %s\n", id, typeName, severityName, sourceName, message);
}

static unsigned int compileShader(const char *filepath, unsigned int type)
{
    FILE *file = fopen(filepath, "r");

    fseek(file, 0, SEEK_END);
    int length = ftell(file);
    rewind(file);

    char buffer[length];
    fread(buffer, 1, length, file);

    fclose(file);

    const char *source = buffer;

    unsigned int shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, &length);
    glCompileShader(shader);

    int result;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);

        char message[length];
        glGetShaderInfoLog(shader, length, &length, message);

        printf("Failed to compile %s shader\n",
                type == GL_VERTEX_SHADER ? "vertex" :
                type == GL_FRAGMENT_SHADER ? "fragment" :
                type == GL_COMPUTE_SHADER ? "compute" : "other");

        printf("%s\n", message);

        glDeleteShader(shader);
        exit(1);
    }

    return shader;
}

static unsigned int createProgram(char **filepaths, unsigned int *types, int count)
{
    unsigned int program = glCreateProgram();

    unsigned int shaders[count];

    for (int i = 0; i < count; i++)
    {
        shaders[i] = compileShader(filepaths[i], types[i]);
        glAttachShader(program, shaders[i]);
    }

    glLinkProgram(program);
    glValidateProgram(program);

    for (int i = 0; i < count; i++)
    {
        glDeleteShader(shaders[i]);
    }

    return program;
}

Renderer *renderSetup(unsigned int screenWidth, unsigned int screenHeight)
{
    static Renderer renderer;
    static bool initialized = false;

    assert(!initialized);
    initialized = true;

    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "failed to initialize glew\n");
    }

    glDebugMessageCallback(debugCallback, NULL);

    printf("%s\n", glGetString(GL_VERSION));

    float verticies[] = {
        -1.0f, -1.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 1.0f, 1.0f,
        -1.0f,  1.0f, 0.0f, 1.0f,
    };

    unsigned int indices[] = {
        0, 1, 2,
        2, 3, 0,
    };

    unsigned int vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, 4 * 4 * sizeof(float), verticies, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void *) (sizeof(float) * 2));
    unsigned int indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);

    unsigned int computeProgram = createProgram((char *[]) {"shaders/shader.comp"}, (unsigned int[]) {GL_COMPUTE_SHADER}, 1);
    unsigned int shaderProgram = createProgram((char *[]) {"shaders/shader.vert", "shaders/shader.frag"}, (unsigned int[]) {GL_VERTEX_SHADER, GL_FRAGMENT_SHADER}, 2);

    unsigned int texture;
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindImageTexture(0, texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

    unsigned int ssbo;
    glGenBuffers(1, &ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, WORLD_BINDING, ssbo);

    renderer = (Renderer) {
        .screenWidth = screenWidth,
        .screenHeight = screenHeight,
        .computeProgram = computeProgram,
        .shaderProgram = shaderProgram,
        .world_ssbo = ssbo,
    };

    return &renderer;
}

void renderFrame(Renderer *renderer, Camera camera, Octree world)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(renderer->computeProgram);

    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(OctreeNode) * world.nodesLength, world.nodes, GL_DYNAMIC_DRAW);
    glUniform4f(ROTATION_LOCATION, camera.rotation[0], camera.rotation[1], camera.rotation[2], camera.rotation[3]);
    glUniform3f(POSITION_LOCATION, camera.position[0], camera.position[1], camera.position[2]);
    glUniform1ui(OCTREE_LEVELS_POSITION, world.levels);

    glDispatchCompute(ceil(renderer->screenWidth / 8.0f), ceil(renderer->screenHeight / 8.0f), 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    glUseProgram(renderer->shaderProgram);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
}

void renderCleanup(Renderer *renderer)
{
    glDeleteBuffers(1, &renderer->world_ssbo);

    glDeleteProgram(renderer->computeProgram);
    glDeleteProgram(renderer->shaderProgram);
}
