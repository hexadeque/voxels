#ifndef WORLD_H
#define WORLD_H

#include "octree.h"

Octree worldCreate(unsigned int levels, Arena *arena);

#endif
