#include "octree.h"

#include <math.h>
#include <string.h>

static OctreeNode createNode(Octree *octree, WorldGenerator worldGenerator, void *data, unsigned int x, unsigned int y, unsigned int z, unsigned int level, Arena *arena)
{
    if (level == 0)
    {
        return (OctreeNode) { .type = worldGenerator(x, y, z, data) };
    }

    OctreeNode children[8];
    unsigned int nodeWidth = pow(2, level - 1);

    for (size_t i = 0; i < 8; i++)
    {
        unsigned int dx = i % 2;
        unsigned int dy = (i / 2) % 2;
        unsigned int dz = i / 4;

        children[i] = createNode(octree, worldGenerator, data, x + dx * nodeWidth, y + dy * nodeWidth, z + dz * nodeWidth, level - 1, arena);
    }

    bool homogenous = children[0].type != Mixed;

    for (size_t i = 1; homogenous && i < 8; i++)
    {
        homogenous = children[i].type == children[0].type;
    }

    if (homogenous)
    {
        return (OctreeNode) { .type = children[0].type };
    }

    const size_t size = 8 * sizeof(OctreeNode);
    memcpy(arenaAlloc(arena, size), children, size);

    unsigned int start = octree->nodesLength;
    octree->nodesLength += 8;

    return (OctreeNode) {
        .type = Mixed,
        .children = { start, start + 1, start + 2, start + 3, start + 4, start + 5, start + 6, start + 7 },
    };
}

Octree octreeCreate(WorldGenerator worldGenerator, unsigned int depth, void *data, Arena *arena)
{
    Octree octree = {
        .nodes = arenaAlloc(arena, sizeof(OctreeNode)),
        .nodesLength = 1,
        .levels = depth,
    };

    OctreeNode root = createNode(&octree, worldGenerator, data, 0, 0, 0, depth, arena);
    octree.nodes[0] = root;

    return octree;
}
